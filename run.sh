export LOGGER_FILENAME="./logger.log"

#main.go
export KUMPARAN_ARTICLE_CRUD_PORT=0.0.0.0:8144
export MAXPROCS="1"

#routes/route.go
export KUMPARAN_ARTICLE_CRUD="KUMPARAN_ARTICLE_CRUD"
export READ_TIMEOUT="120"
export WRITE_TIMEOUT="120"

#db/postgres.go
export DB_POSTGRES_USER=root
export DB_POSTGRES_PASS=
export DB_POSTGRES_NAME="kumparan"
export DB_POSTGRES_ADDRESS="127.0.0.1"
export DB_POSTGRES_PORT="3307"
export DB_POSTGRES_DEBUG="true"
export DB_POSTGRES_TYPE="postgres"
export DB_POSTGRES_SSL_MODE="disable"

#router/routers.go
#swagger
export APPS_ENV="local"
export SWAGGER_HOST_LOCAL="localhost:8144"
export SWAGGER_HOST_DEV="13.228.26.86:8144"

#redis
export REDIS_MASTER="17.228.23.160:8079;17.228.23.160:8078;17.228.23.160:8077"
export REDIS_SLAVE="23.228.23.160:6479;23.228.23.160:6478;23.228.23.160:6477"
export REDIS_KEY_CACHE_ARTICLE="ARTICLE:ID:"

#kafka
export KAFKA_BROKERS="17.229.100.66:7097,17.229.100.66:7098,17.229.100.66:7099"
export DEV_KAFKA_TOPICS="DEV-article"
export STAG_KAFKA_TOPICS="STAG-article"
export PROD_KAFKA_TOPICS="PROD-article"
export TYPE="DEV"

go run main.go
