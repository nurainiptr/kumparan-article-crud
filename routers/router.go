package routers

import (
	"fmt"
	"io"
	"kumparan-article-crud/docs"
	v001Controller "kumparan-article-crud/v0.0.1/controllers"
	"os"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/kelseyhightower/envconfig"
	"github.com/opentracing/opentracing-go"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/uber/jaeger-client-go"
	"ottodigital.id/library/httpserver/ginserver"
	tracing "ottodigital.id/library/ottotracing"
	"ottodigital.id/library/utils"
)

// ServerEnv ..
type ServerEnv struct {
	ServiceName     string `envconfig:"SERVICE_NAME" default:"KUMPARAN-ARTICLE-CRUD"`
	OpenTracingHost string `envconfig:"OPEN_TRACING_HOST" default:"13.250.21.165:5775"`
	DebugMode       string `envconfig:"DEBUG_MODE" default:"debug"`
	ReadTimeout     int    `envconfig:"READ_TIMEOUT" default:"120"`
	WriteTimeout    int    `envconfig:"WRITE_TIMEOUT" default:"120"`
}

var (
	server ServerEnv
)

func init() {
	if err := envconfig.Process("SERVER", &server); err != nil {
		fmt.Println("Failed to get SERVER env:", err)
	}
}

// Server ..
func Server(listenAddress string) error {
	oRouter := ORouter{}
	oRouter.InitTracing()
	oRouter.Routers()
	defer oRouter.Close()

	err := ginserver.GinServerUp(listenAddress, oRouter.Router)
	if err != nil {
		fmt.Println("Error:", err)
		return err
	}

	fmt.Println("Server UP")
	return nil
}

// ORouter ..
type ORouter struct {
	Tracer   opentracing.Tracer
	Reporter jaeger.Reporter
	Closer   io.Closer
	Err      error
	GinFunc  gin.HandlerFunc
	Router   *gin.Engine
}

// Routers ..
func (oRouter *ORouter) Routers() {
	gin.SetMode(server.DebugMode)

	// programmatically set swagger info
	docs.SwaggerInfo.Title = "kumparan-article-crud API"
	docs.SwaggerInfo.Description = "<kumparan-article-crud description>"
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Schemes = []string{"http"}
	switch utils.GetEnv("APPS_ENV", "local") {
	case "local":
		docs.SwaggerInfo.Host = utils.GetEnv("SWAGGER_HOST_LOCAL", "localhost:8044")
	case "dev":
		docs.SwaggerInfo.Host = utils.GetEnv("SWAGGER_HOST_DEV", "13.228.25.85:8044")
	}

	router := gin.New()
	router.Use(oRouter.GinFunc)
	router.Use(gin.Recovery())

	router.Use(cors.New(cors.Config{
		AllowMethods:     []string{"GET", "POST", "OPTIONS", "DELETE", "PUT"},
		AllowHeaders:     []string{"Origin", "authorization", "Content-Length", "Content-Type", "User-Agent", "Referrer", "Host", "Token"},
		ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Content-Type"},
		AllowCredentials: true,
		AllowAllOrigins:  true,
		MaxAge:           86400,
	}))

	v1 := router.Group("/kumparan/v0.0.1")
	{
		v1.POST("/articles", v001Controller.Create)
	}

	// use ginSwagger middleware to serve the API docs
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	oRouter.Router = router

}

// InitTracing ..
func (oRouter *ORouter) InitTracing() {
	hostName, err := os.Hostname()
	if err != nil {
		hostName = "PROD"
	}

	tracer, reporter, closer, err := tracing.InitTracing(fmt.Sprintf("%s::%s", server.ServiceName, hostName), server.OpenTracingHost, tracing.WithEnableInfoLog(true))
	if err != nil {
		fmt.Println("Error :", err)
	}
	opentracing.SetGlobalTracer(tracer)

	oRouter.Closer = closer
	oRouter.Reporter = reporter
	oRouter.Tracer = tracer
	oRouter.Err = err
	oRouter.GinFunc = tracing.OpenTracer([]byte("api-request-"))
}

// Close ..
func (oRouter *ORouter) Close() {
	oRouter.Closer.Close()
	oRouter.Reporter.Close()
}
