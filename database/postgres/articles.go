package postgres

import (
	"kumparan-article-crud/database/dbmodels"
)

// Save data Article
func (database *DbPostgres) Save(articleRequest dbmodels.Articles) (articleResponse dbmodels.Articles, err error) {
	err = Dbcon.Save(&articleRequest).Scan(&articleResponse).Error
	if err != nil {
		return articleResponse, err
	}
	return articleResponse, nil
}
