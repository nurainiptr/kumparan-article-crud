package postgres

import (
	"fmt"
	"github.com/astaxie/beego/logs"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres" // Postgres some standard stuff
	"github.com/kelseyhightower/envconfig"
	"kumparan-article-crud/models"
)

// DbEnv ..
type DbEnv struct {
	DbUser   string `envconfig:"DB_POSTGRES_USER" default:"root"`
	DbPass   string `envconfig:"DB_POSTGRES_PASS" default:""`
	DbName   string `envconfig:"DB_POSTGRES_NAME" default:"kumparan"`
	DbAddres string `envconfig:"DB_POSTGRES_ADDRESS" default:"127.0.0.1"`
	DbPort   string `envconfig:"DB_POSTGRES_PORT" default:"3307"`
	DbDebug  bool   `envconfig:"DB_POSTGRES_DEBUG" default:"true"`
	DbType   string `envconfig:"DB_POSTGRES_TYPE" default:"postgres"`
	SslMode  string `envconfig:"DB_POSTGRES_SSL_MODE" default:"disable"`
}

var (
	// Dbcon ..
	Dbcon *gorm.DB

	// Errdb ..
	Errdb error
	dbEnv DbEnv
)

func init() {
	err := envconfig.Process("Database_Lemonilo", &dbEnv)
	if err != nil {
		fmt.Println("Failed to get Database Lemonilo env:", err)
	}

	if DbOpen() != nil {
		fmt.Println("Can't Open ", dbEnv.DbName, " DB", DbOpen())
	}
	Dbcon = GetDbCon()
	Dbcon = Dbcon.LogMode(true)
}

// DbOpen ..
func DbOpen() error {
	args := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s", dbEnv.DbAddres, dbEnv.DbPort, dbEnv.DbUser, dbEnv.DbPass, dbEnv.DbName, dbEnv.SslMode)
	Dbcon, Errdb = gorm.Open("postgres", args)
	if Errdb != nil {
		logs.Error(fmt.Sprintf("Open db Err ", Errdb))
		return Errdb
	}

	if errping := Dbcon.DB().Ping(); errping != nil {
		logs.Error(fmt.Sprintf("Test Ping because Db Not Connected :", errping))
		fmt.Println("Can't Open Db")
		return errping
	}
	logs.Info("Connect Db successful")
	return nil
}

// GetDbCon ..
func GetDbCon() *gorm.DB {
	if errping := Dbcon.DB().Ping(); errping != nil {
		logs.Error(fmt.Sprintf("Test Ping because Db Not Connected :", errping))
		if errping = DbOpen(); errping != nil {
			logs.Error(fmt.Sprintf("Try to connect again but error :", errping))
		}
	}
	Dbcon.LogMode(true)
	return Dbcon
}

// DbPostgres ..
type DbPostgres struct {
	General models.GeneralModel
}
