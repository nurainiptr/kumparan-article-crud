package dbmodels

import "time"

// Articles ..
type Articles struct {
	Id      int       `gorm:"column:id;primary_key" json:"id" example:"1"`
	Author  string    `gorm:"column:author" json:"author" example:"putri"`
	Title   string    `gorm:"column:title" json:"title"  example:"Terkonfirmasi Covid-19, Wakil Duta Besar RI untuk India Meninggal Dunia"`
	Body    string    `gorm:"column:body" json:"body" example:"Wakil Duta Besar Republik Indonesia (Wadubes RI) untuk India, Ferdy Nico Yohannes Piay, meninggal dunia setelah terkonfirmasi Covid-19. Informasi tentang meninggalnya ..."`
	Created time.Time `gorm:"column:created" json:"created" example:"12/12/2021"`
}
