package services

import (
	"encoding/json"
	"fmt"
	"kumparan-article-crud/constants"
	"kumparan-article-crud/database/dbmodels"
	kafka "kumparan-article-crud/kafka"
	kafkaService "kumparan-article-crud/kafka/services"
	"kumparan-article-crud/models"
	redis "kumparan-article-crud/rediscluster"
	"kumparan-article-crud/utils"
	utls "ottodigital.id/library/utils"
	"time"
)

// Create to create new article
func (svc *Service) Create(form models.ArticleCreateForm, res *models.Response) {

	article := dbmodels.Articles{
		Author:  form.Author,
		Title:   form.Title,
		Body:    form.Body,
		Created: time.Now(),
	}

	data, err := svc.Database.Save(article)
	if err != nil {
		res.Meta = utils.GetMetaResponse("article.create.failed")
		return
	}

	//cache
	redisKeyCacheArticle := utls.GetEnv("REDIS_KEY_CACHE_ARTICLE", "ARTICLE:ID:")
	go redis.SaveRedis(redisKeyCacheArticle+fmt.Sprintf("%d", data.Id), data)

	//push to broker
	kafka := kafkaService.ServicePush{
		Topic:       kafka.GetTopic(),
		Brokerlist:  kafka.GetKafkaBroker(),
		ConfigKafka: kafka.GetConfigKafka(),
	}
	formInBytes, errs := json.Marshal(data)
	if errs != nil {
		return
	}
	err = kafka.Push(formInBytes)
	if err != nil {
		return
	}

	res.Data = data
	res.Meta = utils.GetMetaResponse(constants.KeyResponseSuccessful)
	return
}
