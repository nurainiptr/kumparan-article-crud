package services

import (
	"kumparan-article-crud/models"
	"reflect"
	"testing"
)

func TestService_Create(t *testing.T) {
	type fields struct {
		General models.GeneralModel
	}
	type args struct {
		params   models.ArticleCreateForm
		response *models.Response
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   models.Response
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			service := &Service{
				General: tt.fields.General,
			}
			if service.Create(tt.args.params, tt.args.response); !reflect.DeepEqual(tt.args.response, tt.want) {
				t.Errorf("service.Create = %v, want %v", tt.args.response, tt.want)
			}
		})
	}
}
