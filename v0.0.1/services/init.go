package services

import (
	db "kumparan-article-crud/database/postgres"
	"kumparan-article-crud/models"
	lg "ottodigital.id/library/logger/v2"
)

// Service ..
type Service struct {
	General  models.GeneralModel
	Log      lg.OttologInterface
	Database db.DbPostgres
}

// ServiceInterface ..
type ServiceInterface interface {
	Create(models.ArticleCreateForm, *models.Response)
}

// InitiateService ..
func InitiateService(log lg.OttologInterface) ServiceInterface {
	return &Service{
		Log: log,
	}
}
