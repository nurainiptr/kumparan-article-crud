package controllers

import (
	"github.com/stretchr/testify/assert"
	"kumparan-article-crud/models"
	"kumparan-article-crud/v0.0.1/services"
	lg "ottodigital.id/library/logger/v2"
	"testing"
)

func Test_Create(t *testing.T) {
	var lg lg.OttologInterface
	var res *models.Response

	req := models.ArticleCreateForm{
		Author: "putri",
		Title:  "Terkonfirmasi Covid-19, Wakil Duta Besar RI untuk India Meninggal Dunia",
		Body:   "Wakil Duta Besar Republik Indonesia (Wadubes RI) untuk India, Ferdy Nico Yohannes Piay, meninggal dunia setelah terkonfirmasi Covid-19. Informasi tentang meninggalnya ...",
	}

	services.InitiateService(lg).Create(req, res)
	assert.Equal(t, 200, res.Meta.Code, "OK response is expected")
}
