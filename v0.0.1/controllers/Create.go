package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"kumparan-article-crud/constants"
	"kumparan-article-crud/models"
	"kumparan-article-crud/utils"
	"kumparan-article-crud/v0.0.1/services"
	"net/http"
	lg "ottodigital.id/library/logger/v2"
)

// Create data Article
// Article - Create godoc
// @Summary Article - Create
// @Description Article - Create
// @ID Article - Create
// @Tags Article
// @Router /kumparan/v0.0.1/articles [post]
// @Accept json
// @Produce json
// @Param Body body models.ArticleCreateForm true "Body"
// @Success 200 {object} models.Response{} "Article - Create EXAMPLE"
func Create(ctx *gin.Context) {
	log := lg.InitLogs(ctx.Request)

	res := models.Response{
		Meta: utils.GetMetaResponse(constants.KeyResponseDefault),
	}

	req := models.ArticleCreateForm{}
	if err := ctx.ShouldBindJSON(&req); err != nil {
		go log.Error(fmt.Sprintf("Body request error: %v", err))
		ctx.JSON(http.StatusBadRequest, res)
		return
	}

	reqBytes, _ := json.Marshal(req)
	log.Info("Article-Create  Controller",
		log.AddField("RequestBody:", string(reqBytes)))

	services.InitiateService(log).Create(req, &res)

	resBytes, _ := json.Marshal(res)
	log.Info("Article-Create Controller",
		log.AddField("ResponseBody: ", string(resBytes)))

	ctx.JSON(http.StatusOK, res)
}
