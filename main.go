package main

import (
	"fmt"
	"github.com/astaxie/beego/logs"
	"go.uber.org/zap"
	"kumparan-article-crud/routers"
	"os"
	"os/signal"
	"ottodigital.id/library/utils"
	"runtime"
	"strconv"
	"syscall"
)

var (
	listenAddrApi string
)

func main() {
	maxProc, _ := strconv.Atoi(utils.GetEnv("MAXPROCS", "1"))

	runtime.GOMAXPROCS(maxProc)

	var errChan = make(chan error, 1)

	go func() {
		listenAddress := utils.GetEnv("KUMPARAN_ARTICLE_CRUD_PORT", "0.0.0.0:8144")

		fmt.Println("Starting @", listenAddress)
		logs.Info(fmt.Sprintf("Starting @ ", listenAddress))
		errChan <- routers.Server(listenAddress)

	}()

	var signalChan = make(chan os.Signal, 1)
	signal.Notify(signalChan, os.Interrupt, syscall.SIGTERM)
	select {
	case <-signalChan:
		fmt.Println("got an interrupt, exiting...")
		logs.Error("Got an interrupt, exiting...")
	case err := <-errChan:
		if err != nil {
			fmt.Println("error while running api, exiting...", err)
			logs.Error("Error while running api, exiting... ", zap.Error(err))
		}
	}
}
