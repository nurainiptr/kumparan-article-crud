package kafka

import (
	"github.com/Shopify/sarama"
	"log"
	"os"
	"ottodigital.id/library/utils"
	"strconv"
	"strings"
	"time"
)

var (
	typeWorker      string
	kafkaBrokerUrl  string
	kafkaTopic      string
	kafkaTopicsDEV  string
	kafkaTopicsSTAG string
	kafkaTopicsPROD string
	kafkaClient     string
	kafkaTimeout    int
	maxmsgbyte      int
)

func init() {
	kafkaBrokerUrl = utils.GetEnv("KAFKA_BROKERS", "17.229.100.66:7097,17.229.100.66:7098,17.229.100.66:7099")
	typeWorker = utils.GetEnv("TYPE", "DEV")
	kafkaTopicsDEV = utils.GetEnv("DEV_KAFKA_TOPICS", "DEV-article")
	kafkaTopicsSTAG = utils.GetEnv("STAG_KAFKA_TOPICS", "STAG-article")
	kafkaTopicsPROD = utils.GetEnv("PROD_KAFKA_TOPICS", "PROD-article")
	kafkaClient = utils.GetEnv("KAFKA_CLIENT", "client-article")
	kafkaTimeout, _ = strconv.Atoi(utils.GetEnv("KAFKA_TIMEOUT", "10"))
	maxmsgbyte, _ = strconv.Atoi(utils.GetEnv("MAXMSGBYTES", "50000000"))
}

// GetConectionKafka ..
func GetConectionKafka(brokerList string, config *sarama.Config) KafkaProducer {
	producer, err := sarama.NewSyncProducer(strings.Split(brokerList, ","), config)
	return KafkaProducer{
		Connection: producer,
		ErrRes:     err,
	}
}

// GetKafkaBroker ..
func GetKafkaBroker() string {
	return kafkaBrokerUrl
}

// GetTopic ..
func GetTopic() string {
	switch typeWorker {
	case "DEV":
		kafkaTopic = kafkaTopicsDEV
		break
	case "STAG":
		kafkaTopic = kafkaTopicsSTAG
		break
	case "PROD":
		kafkaTopic = kafkaTopicsPROD
		break
	}
	return kafkaTopic
}

// GetConnKakfaProcedure ..
func GetConnKakfaProcedure() KafkaProducer {
	producer, err := sarama.NewSyncProducer(strings.Split(GetKafkaBroker(), ","), GetConfigKafka())
	return KafkaProducer{
		Connection: producer,
		ErrRes:     err,
	}
}

// GetConfigKafka ..
func GetConfigKafka() *sarama.Config {
	sarama.Logger = log.New(os.Stdout, "[sarama] ", log.LstdFlags)
	configKafka := sarama.NewConfig()
	configKafka.Producer.Return.Successes = true
	configKafka.Producer.Partitioner = sarama.NewRandomPartitioner
	configKafka.ClientID = kafkaClient
	configKafka.Producer.MaxMessageBytes = maxmsgbyte
	configKafka.Producer.Timeout = 5 * time.Second
	configKafka.Net.DialTimeout = 2 * time.Second
	configKafka.Net.ReadTimeout = 5 * time.Second
	configKafka.Net.WriteTimeout = 5 * time.Second

	return configKafka
}
