package services

import (
	"errors"
	"github.com/Shopify/sarama"
	"github.com/astaxie/beego/logs"
	"kumparan-article-crud/kafka"
)

// ServicePush ..
type ServicePush struct {
	Topic       string
	Brokerlist  string
	ConfigKafka *sarama.Config
}

// Push to produce to kafka
func (s *ServicePush) Push(bdata []byte) error {
	logs.Info("Produce to Kafka")
	kafkaProducer := kafka.GetConectionKafka(s.Brokerlist, s.ConfigKafka)
	if kafkaProducer.ErrRes != nil {
		logs.Error("Err Client REQ :", kafkaProducer.ErrRes)
		return kafkaProducer.ErrRes
	}
	kafkaProducer.Topic = s.Topic
	kafkaProducer.Value = sarama.ByteEncoder(bdata)
	kafkaProducer.Process()
	if kafkaProducer.Done == false {
		return errors.New("Sending to Kafka Failed")
	}
	return nil
}

// CheckConnectionKafka ..
func (s *ServicePush) CheckConnectionKafka() (bool, error) {
	kafkaProducer := kafka.GetConectionKafka(s.Brokerlist, s.ConfigKafka)
	if kafkaProducer.ErrRes != nil {
		return false, kafkaProducer.ErrRes
	}
	return true, kafkaProducer.ErrRes
}
