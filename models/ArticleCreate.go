package models

// ArticleCreateForm ..
type ArticleCreateForm struct {
	Author string `json:"author" example:"putri"`
	Title  string `json:"title" example:"Terkonfirmasi Covid-19, Wakil Duta Besar RI untuk India Meninggal Dunia"`
	Body   string `json:"body" example:"Wakil Duta Besar Republik Indonesia (Wadubes RI) untuk India, Ferdy Nico Yohannes Piay, meninggal dunia setelah terkonfirmasi Covid-19. Informasi tentang meninggalnya ..."`
}
