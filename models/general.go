package models

import (
	"context"
	"github.com/opentracing/opentracing-go"
	"go.uber.org/zap"
)

// GeneralModel ..
type GeneralModel struct {
	ParentSpan opentracing.Span
	Zaplog     *zap.Logger
	SpanId     string
	Context    context.Context
}
